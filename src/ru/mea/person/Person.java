package ru.mea.person;

public abstract  class Person {
    private String surname;
    private Gender gender;
    private int year;

    Person(String surname, Gender gender, int year) {
        this.surname = surname;
        this.gender = gender;
        this.year = year;
    }

    String getSurname() {
        return surname;
    }




    public Gender getGender() {
        return gender;
    }



    public int getYear() {
        return year;
    }

    @Override
    public  String toString() {
        return "Person{" +
                "surname='" + surname + '\'' +
                ", gender=" + gender +
                ", year=" + year +
                '}';
    }
}
