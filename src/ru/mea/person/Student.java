package ru.mea.person;

public class Student extends  Person {
    private int yearOfReceipt;
    private String specialtyCode;


    Student(String surname, Gender gender, int year, int yearOfReceipt, String specialtyCode) {
        super(surname, gender, year);
        this.yearOfReceipt = yearOfReceipt;
        this.specialtyCode = specialtyCode;
    }

    int getYearOfReceipt() {
        return yearOfReceipt;
    }


   String getSpecialtyCode() {
        return specialtyCode;
    }





    @Override
    public String toString() {
        return "Student{" + " Фамилия " + getSurname() + " Год рождения " + getYear() + " Пол " + getGender() +
                " yearOfReceipt=" + yearOfReceipt +
                " , specialtyCode='" + specialtyCode + '\'' +
                '}';
    }
}
