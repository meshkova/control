package ru.mea.person;

public class Demo {
    public static void main(String[] args) {


        Student vashina = new Student("Вашина", Gender.FEMALE, 2002, 2015, "09.02.07");
        Student mishina = new Student("Мишина", Gender.FEMALE, 2003, 2018, "09.02.07");
        Student lyshnikov = new Student("Лушников", Gender.MALE, 2001, 2017, "10.02.09");

        Student[] array = {vashina, mishina, lyshnikov};
        System.out.println(array[0]);

    }

    /**
     * Поиск студента по указанным параметрам
     *
     * @param array массив
     */
    private static void search(Student[] array) {

        for (int i = 0; i < array.length; i++) {
            if (array[i].getGender() == Gender.FEMALE && array[i].getYearOfReceipt() == 2018 && array[i].getSpecialtyCode().equals("09.02.07")) {
                System.out.println(array[i]);
            }
        }

    }
}