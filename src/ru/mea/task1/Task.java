package ru.mea.task1;

import java.util.Arrays;

public class Task {
    private static final int max = 80;
    private static final int min = 50;

    public static void main(String[] args) {
        int[] array = new int[10];
        arrayFilling(array);
        System.out.println(Arrays.toString(array));
        System.out.println(search(array));

    }

    /**
     * Метод, реализующий рандомное заполнение массива
     * @param array массив
     */
    private static void arrayFilling(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * ((max - min) + 1)) + min;

        }
    }

    /**
     *  Метод , реализующий поиск минимального элмента
     * @param array массив
     * @return минимальный элемент
     */
    private static int search(int[] array) {
        int minElement = array[0];
        for (int i = 0; i < array.length; i++) {
            if (minElement > array[i]) {
                minElement = array[i];
            }
        }
        return minElement;
    }
}
